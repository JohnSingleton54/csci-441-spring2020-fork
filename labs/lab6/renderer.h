#ifndef _CSCI441_RENDERER_H_
#define _CSCI441_RENDERER_H_

#define BUFFER_OFFSET(offset)  ((void *)(offset))  // See p. 27 of GL Guide.

#include <csci441/matrix3.h>

class Renderer {
    Matrix3 itModel_tmp;
public:

    void render(const Camera& camera, Model& m, const Vector4& light) {
        itModel_tmp.inverse_transpose(m.model);

        m.shader.use();
        Uniform::set(m.shader.id(), "model", m.model);
        Uniform::set(m.shader.id(), "projection", camera.projection);
        Uniform::set(m.shader.id(), "camera", camera.look_at());
        Uniform::set(m.shader.id(), "eye", camera.eye);
        Uniform::set(m.shader.id(), "lightPos", light);
        Uniform::set(m.shader.id(), "itModel", itModel_tmp);

        // render the cube
        //glBindVertexArray(m.vao);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m.ibo);
        //glDrawArrays(GL_TRIANGLES, 0, m.size);
        glDrawElements(GL_TRIANGLES, m.indices.size(), GL_UNSIGNED_INT, BUFFER_OFFSET(0));
    }
};

#endif
