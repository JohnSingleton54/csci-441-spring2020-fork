// John M. Singleton, collaborator: Yin YaLan
// CSCI 441 - Computer Graphics
// Lab 2
// due: T 2/4/2020 by 4:30 PM

// References: various YouTube videos on channel The Cherno

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

/**
 * BELOW IS A BUNCH OF HELPER CODE
 * You do not need to understand what is going on with it, but if you want to
 * know, let me know and I can walk you through it.
 */

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

GLFWwindow* initWindow() {
    GLFWwindow* window;
    if (!glfwInit()) {
        return NULL;
    }

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    window = glfwCreateWindow(640, 480, "Lab 2", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return NULL;
    }

    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return NULL;
    }

    return window;
}

std::string shaderTypeName(GLenum shaderType) {
    switch(shaderType) {
        case GL_VERTEX_SHADER: return "VERTEX";
        case GL_FRAGMENT_SHADER: return "FRAGMENT";
        default: return "UNKNOWN";
    }
}

std::string readFile(const std::string& fileName) {
    std::ifstream stream(fileName);
    std::stringstream buffer;
    buffer << stream.rdbuf();

    std::string source = buffer.str();
    std::cout << "Source:" << std::endl;
    std::cout << source << std::endl;

    return source;
}

/** END OF CODE THAT YOU DON'T NEED TO WORRY ABOUT */

GLuint createShader(const std::string& fileName, GLenum shaderType) {
    std::string source = readFile(fileName);
    const char* src_ptr = source.c_str();

    /** YOU WILL ADD CODE STARTING HERE */
    //GLuint shader = 0;
    // create the shader using
    // glCreateShader, glShaderSource, and glCompileShader

    GLuint shader = glCreateShader(shaderType);
    glShaderSource(shader, 1, &src_ptr, nullptr);
    glCompileShader(shader);

    /** END CODE HERE */

    // Perform some simple error handling on the shader
    int success;
    char infoLog[512];
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        std::cerr << "ERROR::SHADER::" << shaderTypeName(shaderType)
            <<"::COMPILATION_FAILED\n"
            << infoLog << std::endl;
    }

    return shader;
}

GLuint createShaderProgram(GLuint vertexShader, GLuint fragmentShader) {
    /** YOU WILL ADD CODE STARTING HERE */
    // create the program using glCreateProgram, glAttachShader, glLinkProgram
    GLuint program = glCreateProgram();
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);
    /** END CODE HERE */

    // Perform some simple error handling
    int success;
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) {
        char infoLog[512];
        glGetProgramInfoLog(program, 512, NULL, infoLog);
        std::cerr << "ERROR::PROGRAM::COMPILATION_FAILED\n"
            << infoLog << std::endl;
    }

    return program;
}

// This function converts the window coordinates in the array wCoords into normalized device
// coordinates which are put in ndCoords.
void w2nd(float wCoords[3][2], int width, int height, float ndCoords[3][5]){
  for (int i = 0; i < 3; i++){
    ndCoords[i][0] = -1 + wCoords[i][0]*(2/(float)width); // the x-coordinate of Point i
    ndCoords[i][1] = 1 - wCoords[i][1]*(2/(float)height); // the y-coordinate of Point i
  }
}

int main(void) {
    GLFWwindow* window = initWindow();
    if (!window) {
        std::cout << "There was an error setting up the window" << std::endl;
        return 1;
    }

    /** YOU WILL ADD DATA INITIALIZATION CODE STARTING HERE */

    /* PART1: ask the user for coordinates and colors, and convert to normalized
     * device coordinates */

    // convert the triangle to an array of floats containing
    // normalized device coordinates and color components.
    // float triangle[] = ...

    /*
      Prompt the user for 3 points separated by whitespace.
    
      Each point consists of an x-coordinate, a y-coordinate, an r value (0-1), a g value (0-1), and
      a b value (0-1).
    */

    std::cout << "Enter 3 points on 3 separate lines (enter a point as x y r g b):" << std::endl;
    float Ax, Ay, Ar, Ag, Ab, Bx, By, Br, Bg, Bb, Cx, Cy, Cr, Cg, Cb;
    std::cin >> Ax >> Ay >> Ar >> Ag >> Ab >> Bx >> By >> Br >> Bg >> Bb >> Cx >> Cy >> Cr >> Cg >> Cb;
    /*Ax = 50; Ay = 50; Ar = 1; Ag = 0; Ab = 0;
    Bx = 600; By = 20; Br = 0; Bg = 1; Bb = 0;
    Cx = 300; Cy = 400; Cr = 0; Cg = 0; Cb = 1;*/
    /*std::cout << "You entered:" << std::endl;
    std::cout << "Ax: " << Ax << std::endl;
    std::cout << "Ay: " << Ay << std::endl;
    std::cout << "Bx: " << Bx << std::endl;
    std::cout << "By: " << By << std::endl;
    std::cout << "Cx: " << Cx << std::endl;
    std::cout << "Cy: " << Cy << std::endl;*/

    float wCoords[3][2]; int width = 640; int height = 480;
    float ndCoords[3][5] = {{0,0,1,1,1}, {0,0,1,1,1}, {0,0,1,1,1}};
    wCoords[0][0] = Ax; wCoords[0][1] = Ay;
    wCoords[1][0] = Bx; wCoords[1][1] = By;
    wCoords[2][0] = Cx; wCoords[2][1] = Cy;
    w2nd(wCoords, width, height, ndCoords);
    ndCoords[0][2] = Ar;
    ndCoords[0][3] = Ag;
    ndCoords[0][4] = Ab;
    ndCoords[1][2] = Br;
    ndCoords[1][3] = Bg;
    ndCoords[1][4] = Bb;
    ndCoords[2][2] = Cr;
    ndCoords[2][3] = Cg;
    ndCoords[2][4] = Cb;

    std::cout << "The window coordinates of your 3 points are:" << std::endl;
    std::cout << "Ax: " << wCoords[0][0] << std::endl;
    std::cout << "Ay: " << wCoords[0][1] << std::endl;
    std::cout << "Bx: " << wCoords[1][0] << std::endl;
    std::cout << "By: " << wCoords[1][1] << std::endl;
    std::cout << "Cx: " << wCoords[2][0] << std::endl;
    std::cout << "Cy: " << wCoords[2][1] << std::endl;

    std::cout << "The normalized device coordinates of your 3 points are:" << std::endl;
    std::cout << "Ax: " << ndCoords[0][0] << std::endl;
    std::cout << "Ay: " << ndCoords[0][1] << std::endl;
    std::cout << "Ar: " << ndCoords[0][2] << std::endl;
    std::cout << "Ag: " << ndCoords[0][3] << std::endl;
    std::cout << "Ab: " << ndCoords[0][4] << std::endl;

    std::cout << "Bx: " << ndCoords[1][0] << std::endl;
    std::cout << "By: " << ndCoords[1][1] << std::endl;
    std::cout << "Br: " << ndCoords[1][2] << std::endl;
    std::cout << "Bg: " << ndCoords[1][3] << std::endl;
    std::cout << "Bb: " << ndCoords[1][4] << std::endl;

    std::cout << "Cx: " << ndCoords[2][0] << std::endl;
    std::cout << "Cy: " << ndCoords[2][1] << std::endl;
    std::cout << "Cr: " << ndCoords[2][2] << std::endl;
    std::cout << "Cg: " << ndCoords[2][3] << std::endl;
    std::cout << "Cb: " << ndCoords[2][4] << std::endl;    

    float verts[6];
    int index = 0;
    for (int i = 0; i < 3; i++){
        for (int j = 0; j < 2; j++){
            verts[index] = ndCoords[i][j];
            index++;
        }
    }

    std::cout << "verts:" << std::endl;
    std::cout << "verts[0]: " << verts[0] << std::endl;
    std::cout << "verts[1]: " << verts[1] << std::endl;
    std::cout << "verts[2]: " << verts[2] << std::endl;
    std::cout << "verts[3]: " << verts[3] << std::endl;
    std::cout << "verts[4]: " << verts[4] << std::endl;
    std::cout << "verts[5]: " << verts[5] << std::endl;

    /** PART2: map the data */

    // create vertex and array buffer objects using
    // glGenBuffers, glGenVertexArrays
    GLuint VBO[1], VAO[1];
    glGenVertexArrays(1, &VAO[1]);
    glBindVertexArray(VAO[1]);

    // setup triangle using glBindVertexArray, glBindBuffer, GlBufferData

    glGenBuffers(1, &VBO[1]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[1]);
    // *** WHEN I USED verts, THE TRIANGLE DID NOT LOOK RIGHT. WHY????? ***
    glBufferData(GL_ARRAY_BUFFER, 15 * sizeof(float), ndCoords, GL_STATIC_DRAW);

    // setup the attribute pointer for the coordinates
    // setup the attribute pointer for the colors
    // both will use glVertexAttribPointer and glEnableVertexAttribArray;

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (void*)0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (void*)(2 * sizeof(float)));

    /** PART3: create the shader program */

    // create the shaders
    // YOU WILL HAVE TO ADD CODE TO THE createShader FUNCTION ABOVE
    GLuint vertexShader = createShader("../vert.glsl", GL_VERTEX_SHADER);
    GLuint fragmentShader = createShader("../frag.glsl", GL_FRAGMENT_SHADER);

    // create the shader program
    // YOU WILL HAVE TO ADD CODE TO THE createShaderProgram FUNCTION ABOVE
    GLuint shaderProgram = createShaderProgram(vertexShader, fragmentShader);

    // cleanup the vertex and fragment shaders using glDeleteShader
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    /** END INITIALIZATION CODE */

    while (!glfwWindowShouldClose(window)) {
        // you don't need to worry about processInput, all it does is listen
        // for the escape character and terminate when escape is pressed.
        processInput(window);

        /** YOU WILL ADD RENDERING CODE STARTING HERE */
        /** PART4: Implemting the rendering loop */

        // clear the screen with your favorite color using glClearColor
        glClear(GL_COLOR_BUFFER_BIT);

        // set the shader program using glUseProgram
        glUseProgram(shaderProgram);

        // bind the vertex array using glBindVertexArray
        glBindVertexArray(VAO[1]);

        // draw the triangles using glDrawArrays
        glDrawArrays(GL_TRIANGLES, 0, 3);


        /** END RENDERING CODE */

        // Swap front and back buffers
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
