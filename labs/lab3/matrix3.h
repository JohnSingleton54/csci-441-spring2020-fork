#ifndef _CSCI441_MATRIX3_H_
#define _CSCI441_MATRIX3_H_

#include <string>

class Matrix3{

	void init_to_identity(){
		for(int i=0; i<9; ++i){
			if     (i == 0) values[i] = 1;
			else if(i == 4) values[i] = 1;
			else if(i == 8) values[i] = 1;
			else            values[i] = 0;
		}
	}

	public:
		float values[9];

	Matrix3(){
		init_to_identity();
	}

	std::string to_string() const {
		std::ostringstream os;
		for(int i=0; i<9; ++i){
			os << values[i] << " ";
			if( (i+1)%3 == 0 ) os << "\n";
		}
		return os.str();
	}

	void rotate1(float angle){
		values[0] = cos(angle);
		values[1] = -sin(angle);
		values[3] = sin(angle);
		values[4] = cos(angle);
	}

	void scale(float numb){
		float factor = cos(numb);
		values[0] = factor;
		values[4] = factor;
	}

	void translate(float deltaX, float deltaY){
		values[6] = deltaX;
		values[7] = deltaY;
	}

	Matrix3 multiply(Matrix3 second){
		Matrix3 product;
		product.values[0] = values[0]*second.values[0] + values[1]*second.values[3] + values[2]*second.values[6];
		product.values[1] = values[0]*second.values[1] + values[1]*second.values[4] + values[2]*second.values[7];
		product.values[2] = values[0]*second.values[2] + values[1]*second.values[5] + values[2]*second.values[8];
		product.values[3] = values[3]*second.values[0] + values[4]*second.values[3] + values[5]*second.values[6];
		product.values[4] = values[3]*second.values[1] + values[4]*second.values[4] + values[5]*second.values[7];
		product.values[5] = values[3]*second.values[2] + values[4]*second.values[5] + values[5]*second.values[8];
		product.values[6] = values[6]*second.values[0] + values[7]*second.values[3] + values[8]*second.values[6];
		product.values[7] = values[6]*second.values[1] + values[7]*second.values[4] + values[8]*second.values[7];
		product.values[8] = values[6]*second.values[2] + values[7]*second.values[5] + values[8]*second.values[8];
		return product;
	}

	void clear(){
		init_to_identity();
	}
	
};

#endif