// John M. Singleton, collaborators: Tina, William, and Anna
// CSCI 441 - Computer Graphics
// Lab 3
// due: T 2/11/2020 by 4:30 PM

// Reference: https://www.youtube.com/watch?v=L2aiuDDFNIk (for the spacebar part)

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix3.h>

int mode = 0;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

void keyCallback( GLFWwindow *window, int key, int scancode, int action, int mods );


int main(void) {
    /* Initialize the library */
    GLFWwindow* window;
    if (!glfwInit()) {
        return -1;
    }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Lab 3", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    glfwSetKeyCallback( window, keyCallback );

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* init the triangle drawing */
    // define the vertex coordinates of the triangle
    float triangle[] = {
         0.5f,  0.5f, 1.0, 0.0, 0.0,
         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,

         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f, -0.5f, 0.0, 0.0, 1.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,
    };

    // create and bind the vertex buffer object and copy the data to the buffer
    GLuint VBO[1];
    glGenBuffers(1, VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);

    // create and bind the vertex array object and describe data layout
    GLuint VAO[1];
    glGenVertexArrays(1, VAO);
    glBindVertexArray(VAO[0]);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(0*sizeof(float)));
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 5*sizeof(float), (void*)(2*sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    Matrix3 m;
    std::cout << m.to_string() << std::endl;    

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(window);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // use the shader
        shader.use();


        /* Part 2 animate the scene by updating the transformation matrix */

        // update shader uniform

        float timeValue = glfwGetTime();

        switch(mode){
            case 0:  // static: image is non-changing
                m.clear();
                break;
            case 1:  // rotate center: rotate the square around the center of the window
                m.clear();
                m.rotate1(timeValue);
                break;
            case 2:  // rotate off center: rotate the square around a point that is not at the center of the window...
            {        // ...I chose to rotate the square around its upper right corner.
                     // BTW, I had to use the curly braces due to declaring new Matrix3's.
                m.clear();
                Matrix3 temp1;
                temp1.translate(0.5, 0.5);
                Matrix3 temp2;
                temp2.rotate1(timeValue);
                Matrix3 temp3;
                temp3.translate(-0.5, -0.5);
                Matrix3 temp4 = temp3.multiply(temp2);
                m = temp4.multiply(temp1);
            }
                break;
            case 3:  // scale: scale the square
                m.clear();
                m.scale(timeValue);
                break;
            case 4:  // I am still working on the understanding on this one.
            {
                m.clear();
                Matrix3 temp1;
                temp1.translate(0.5, 0.5);
                Matrix3 temp2;
                temp2.rotate1(timeValue);
                Matrix3 temp3;
                temp3.translate(-0.5, -0.5);
                Matrix3 temp4 = temp3.multiply(temp2);
                Matrix3 temp5 = temp4.multiply(temp1);
                Matrix3 temp6;
                temp6.scale(timeValue);
                m = temp5.multiply(temp6);
                break;
            }
            default:
                break;
        }

        int tMatLocation = glGetUniformLocation(shader.id(), "tMat");
        
        glUniformMatrix3fv(tMatLocation, 1, GL_FALSE, m.values);


        // draw our triangles
        glBindVertexArray(VAO[0]);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(triangle));

        /* Swap front and back * buffers */
        glfwSwapBuffers(window);

        /* Poll for and * process * events */
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}

void keyCallback( GLFWwindow *window, int key, int scancode, int action, int mods ){
    //std::cout << key << std::endl;

    if( key == GLFW_KEY_SPACE && action == GLFW_RELEASE ){
        mode = (mode + 1)%5;
        std::cout << "space key is pressed, mode: " << mode << std::endl;

    }
}