#version 330 core
layout (location = 0) in vec2 aPos;
layout (location = 1) in vec3 aColor;

out vec3 myColor;

uniform mat3 tMat;

void main() {
	vec3 position = vec3(aPos.xy, 1.0);
	vec3 new_position = tMat * position;

	gl_Position = vec4(new_position.xy, 1.0, 1.0);

    myColor = aColor;
}
