// John M. Singleton
// CSCI 441 - Computer Graphics
// Lab 0
// T 1/21/2019

#include <iostream> // for Part 4a, etc.
#include <string> // for Part 4b

// for Part 5
class Vector3 {
public:
    float x;
    float y;
    float z;

    // Constructor
    Vector3(float xx, float yy, float zz) : x(xx), y(yy), z(zz) {
        // nothing to do here as we've already initialized x, y, and z above
        std::cout << "in Vector3 constructor" << std::endl;
    }

    // Destructor - called when an object goes out of scope or is destroyed
    ~Vector3() {
        // this is where you would release resources such as memory or file descriptors
        // in this case we don't need to do anything
        std::cout << "in Vector3 destructor" << std::endl;
    }

    Vector3 add(Vector3 v, Vector3 v2){
        Vector3 s(v.x + v2.x, v.y + v2.y, v.z + v2.z);
        return s;
    }
};

int main(int argc, char** argv) {

	// Part 4a - Output
	// I provided the following 2 command-line arguments: "45" and "54".
    std::cout << argv[1] << " " << argv[2] << std::endl;

    // a more general solution, but it currently creates an error:
    /*std::string output1 = "";
    for(int i = 1; i <= argc; i++ ){
        output1 += argv[i] + " ";
    }
    std::cout << output1 << std::endl;*/

	// Part 4b - Input
    std::cout << "What is your name?" << std::endl;

	std::string name;
	std::cin >> name;

	std::cout << "Hello " << name << "." << std::endl;

	// Part 5 - Structs
	Vector3 a(1,2,3);   // allocated to the stack
    Vector3 b(4,5,6);

    /* I could not for the life of me resolve the following error (I tried for > 1 hour):
    main.cpp: In function ‘int main(int, char**)’:
    main.cpp:55:17: error: ‘add’ was not declared in this scope
       55 |     Vector3 c = add(a, b); // copies 6 floats to the arguments in add (3 per Vector3),
          |                 ^~~
    */
    //Vector3 c = add(a, b); // copies 6 floats to the arguments in add (3 per Vector3),
                          // 3 more floats copied into c when it returns
                          // a and b are unchanged

    return 0;
}