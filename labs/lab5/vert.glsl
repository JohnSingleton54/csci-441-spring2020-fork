#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNorm;
layout (location = 2) in vec3 aColor;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 camera;
uniform vec3 lightPos;  // Is there any reason to have this here?

out vec3 fragPos;
out vec3 ourNorm;
out vec3 ourColor;

void main() {
    gl_Position = projection * camera * model * vec4(aPos, 1.0f);
    ourNorm = normalize(vec3((inverse(model) * vec4(aNorm, 1.0f))));
    fragPos = vec3(model * vec4(aPos, 1.0));
    ourColor = aColor;
}
