#version 330 core
in vec3 ourPos;
in vec3 fragPos;
in vec3 ourNorm;
in vec3 ourColor;

uniform mat4 camera;
uniform mat4 model;
uniform vec3 lightPos;
uniform vec3 viewPos;
uniform vec3 eye;

out vec4 fragColor;

void main() {
	float ambientStrength = 0.1f;
	vec3 ambient = ambientStrength * vec3(1.0f, 1.0f, 1.0f);

    //fragColor = vec4(ourColor, 1.0f);


    vec3 lightDir = normalize(lightPos - fragPos);
	float diffuse = max( dot(lightDir, ourNorm), 0.0f );

	float specularStrength = 0.5;
	vec3 viewDir = normalize(eye - fragPos); // eye or viewPos?
	vec3 reflectDir = reflect(-lightDir, ourNorm);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
	vec3 specular = specularStrength * spec * vec3(1.0f, 1.0f, 1.0f);

	vec3 result = (ambient + diffuse + specular) * ourColor;
	fragColor = vec4(result, 1.0);
}
