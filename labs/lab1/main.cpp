// John M. Singleton, collaborator: Yin YaLan
// CSCI 441 - Computer Graphics
// Lab 1
// due: T 1/28/2020 by 4:30 PM

// References:
// 1. "How to return multiple values from a function in C or C++?",
// https://www.geeksforgeeks.org/how-to-return-multiple-values-from-a-function-in-c-or-cpp/

#include <iostream>

#include "bitmap_image.hpp"

bool isInside(float Ax,float Ay,float Bx,float By,float Cx,float Cy,int i,int j,
  float Ar,float Ag,float Ab,float Br,float Bg,float Bb,float Cr,float Cg, float Cb, float c[]){
  float alpha, beta, gamma;
  gamma = ((Ay-By)*i + (Bx-Ax)*j + Ax*By - Bx*Ay) / ((Ay-By)*Cx + (Bx-Ax)*Cy + Ax*By - Bx*Ay);

  beta = ((Ay-Cy)*i + (Cx-Ax)*j + Ax*Cy - Cx*Ay) / ((Ay-Cy)*Bx + (Cx-Ax)*By + Ax*Cy - Cx*Ay);

  alpha = 1 - beta - gamma;

  if (gamma > 0 && gamma < 1 && beta > 0 && beta < 1 && alpha > 0 && alpha < 1){
    c[0] = alpha*Ar + beta*Br + gamma*Cr;
    c[1] = alpha*Ag + beta*Bg + gamma*Cg;
    c[2] = alpha*Ab + beta*Bb + gamma*Cb;
    return true;
  }
  else{
    return false;
  }
}

int main(int argc, char** argv) {
    /*
      Prompt user for 3 points separated by whitespace.
      
      Part 1:
          You'll need to get the x and y coordinate as floating point values
          from the user for 3 points that make up a triangle.

      Part 3:
          You'll need to also request 3 colors from the user each having
          3 floating point values (red, green and blue) that range from 0 to 1.
    */

    std::cout << "Enter 3 points on 3 separate lines (enter a point as x y r g b):" << std::endl;
    float Ax, Ay, Ar, Ag, Ab, Bx, By, Br, Bg, Bb, Cx, Cy, Cr, Cg, Cb;
    std::cin >> Ax >> Ay >> Ar >> Ag >> Ab >> Bx >> By >> Br >> Bg >> Bb >> Cx >> Cy >> Cr >> Cg >> Cb;
    /*std::cout << "You entered:" << std::endl;
    std::cout << "Ax: " << Ax << std::endl;
    std::cout << "Ay: " << Ay << std::endl;
    std::cout << "Bx: " << Bx << std::endl;
    std::cout << "By: " << By << std::endl;
    std::cout << "Cx: " << Cx << std::endl;
    std::cout << "Cy: " << Cy << std::endl;*/

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(640, 480);

    /*
      Part 1:
          Calculate the bounding box of the 3 provided points and loop
          over each pixel in that box and set it to white using:

          rgb_t color = make_color(255, 255, 255);
          image.set_pixel(x,y,color);

      Part 2:
          Modify your loop from part 1. Using barycentric coordinates,
          determine if the pixel lies within the triangle defined by
          the 3 provided points. If it is color it white, otherwise
          move on to the next pixel.

      Part 3:
          For each pixel in the triangle, calculate the color based on
          the calculated barycentric coordinates and the 3 provided
          colors. Your colors should have been entered as floating point
          numbers from 0 to 1. The red, green and blue components range
          from 0 to 255. Be sure to make the conversion.
    */

    float minX, maxX, minY, maxY;
    minX = Ax; maxX = Ax; minY = Ay; maxY = Ay;
    if (Bx < minX) {minX = Bx;}
    if (Cx < minX) {minX = Cx;}
    if (Bx > maxX) {maxX = Bx;}
    if (Cx > maxX) {maxX = Cx;}
    if (By < minY) {minY = By;}
    if (Cy < minY) {minY = Cy;}
    if (By > maxY) {maxY = By;}
    if (Cy > maxY) {maxY = Cy;}
    /*std::cout << "minX: " << minX << std::endl;
    std::cout << "maxX: " << maxX << std::endl;
    std::cout << "minY: " << minY << std::endl;
    std::cout << "maxY: " << maxY << std::endl;*/

    // Part 1 - Bounding box
    /*rgb_t color = make_colour(255, 255, 255);
    for (int i = 0; i < 640; i++){
      for (int j = 0; j < 480; j++){
        if (i >= minX && i <= maxX && j >= minY && j <= maxY){
          image.set_pixel(i, j, color);
        }
      }
    }*/
    //image.set_pixel(50, 10, color);
    //image.set_pixel(25, 20, color);

    // Part 2 - Barycentric coordinates
    /*rgb_t color = make_colour(255, 255, 255);
    for (int i = 0; i < 640; i++){
      for (int j = 0; j < 480; j++){
        if (i >= minX && i <= maxX && j >= minY && j <= maxY){
          if (isInside(Ax, Ay, Bx, By, Cx, Cy, i, j)){
            image.set_pixel(i, j, color);
          }
        }
      }
    }*/

    // Part 3 - Color
    // See, in particular, p. 164 of FoCG. But note that my array c does not correspond to FoCG's
    // vector c.
    float c[3];
    rgb_t color;// = make_colour(255, 255, 255);
    for (int i = 0; i < 640; i++){
      for (int j = 0; j < 480; j++){
        if (i >= minX && i <= maxX && j >= minY && j <= maxY){
          if (isInside(Ax,Ay,Bx,By,Cx,Cy,i,j,Ar,Ag,Ab,Br,Bg,Bb,Cr,Cg,Cb, c)){
            color = make_colour(255*c[0], 255*c[1], 255*c[2]);
            image.set_pixel(i, j, color);
          }
        }
      }
    }


    image.save_image("triangle.bmp");
    std::cout << "Success" << std::endl;
}
