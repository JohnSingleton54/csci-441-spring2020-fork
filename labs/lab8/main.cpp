// John M. Singleton
// CSCI 441 - Computer Graphics
// Lab 8
// due by 4:30 PM on T 4/14/2020 (extension: due by 12:30 AM on W 4/15/2020)
// To switch views, change the value of the global variable typeOfView on Line 13.

#include <iostream>

#include <glm/glm.hpp>

#include <bitmap/bitmap_image.hpp>

int typeOfView = 1; // 0 --> orthographic, 1 --> perspective

struct Viewport {
    glm::vec2 min;
    glm::vec2 max;

    Viewport(const glm::vec2& min, const glm::vec2& max)
        : min(min), max(max) {}
};

struct Sphere {
    int id;
    glm::vec3 center;
    glm::vec3 color;
    float radius;

    Sphere(const glm::vec3& center=glm::vec3(0,0,0),
            float radius=0,
            const glm::vec3& color=glm::vec3(0,0,0))
        : center(center), radius(radius), color(color) {
            static int id_seed = 0;
            id = ++id_seed;
        }
};

struct Ray {
    glm::vec3 origin;
    glm::vec3 direction;

    Ray(const glm::vec3& origin=glm::vec3(0,0,0), const glm::vec3& direction=glm::vec3(0,0,0))
        : origin(origin), direction(glm::normalize(direction)) {}
};

// Part 3b - See pp. 76+77 of FoCG.
int firstIntersection(const Ray& ray, const std::vector<Sphere>& world) {
    // After the for loop, firstSphere will be the index in world of the first sphere intersected by
    // ray.
    int firstSphere = -1;
    // This isn't ideal, but t should be greater than the distance from the origin to any point on
    // the surface of any of the spheres.
    float t = 100.0f;    
    float A, B, C, discriminant, tMinus, tPlus;
    for (int k = 0; k < world.size(); k++) {
        A = glm::dot(ray.direction, ray.direction);
        B = 2*glm::dot(ray.direction, (ray.origin - world[k].center));
        C = glm::dot((ray.origin - world[k].center), (ray.origin - world[k].center)) - 
            world[k].radius*world[k].radius;
        discriminant = B*B - 4*A*C;
        
        // '>' --> '>=' if one wants to count the case where the ray grazes the sphere as an intersection
        if (discriminant > 0) {  // The ray intersects the sphere twice.
            tMinus = (-B - glm::sqrt(discriminant))/(2*A);
            tPlus = (-B + glm::sqrt(discriminant))/(2*A);
            if (tMinus > 0 && tMinus < t) {
                firstSphere = k;
                //std::cout << "firstSphere = " << k << std::endl;
                t = tMinus;
            }
            else if (tPlus > 0 && tPlus < t) {
                firstSphere = k;
                t = tPlus;
            }
        }
    }

    return firstSphere;
}

void render(bitmap_image& image, const std::vector<Sphere>& world) {
    rgb_t color;

    double c[3] = {0.5, 1.0, 0.5}; // the background color

    glm::vec2 min = glm::vec2(-5, -5);
    glm::vec2 max = glm::vec2( 5,  5);
    Viewport viewport = Viewport(min, max);
    float l = viewport.min[0];
    float r = viewport.max[0];
    float b = viewport.max[1];
    float t = viewport.min[1];


    glm::vec3 oDirection = glm::vec3(0, 0, 1);
    glm::vec3 oOrigin;
    float distance = 5.0f;
    glm::vec3 pDirection;
    glm::vec3 pOrigin = glm::vec3(0, 0, -distance);
    std::vector<Ray> oRays;
    std::vector<Ray> pRays;
    float u, v;
    int index;
    int k;

    for (int i = 0; i < 640; i++) {
        for (int j = 0; j < 480; j++) {
            u = l + (r - l)*(i + 0.5f) / 640;
            v = b + (t - b)*(j + 0.5f) / 480;

            index = 480*i + j;
            oOrigin = glm::vec3(u, v, 0);
            oRays.push_back(Ray(oOrigin, oDirection));

            pDirection = glm::vec3(u, v, 0) - pOrigin;
            pRays.push_back(Ray(pOrigin, pDirection));

            //std::cout << "index: " << index << ", oRays.size(): " << oRays.size() << std::endl;

            if (typeOfView == 0) {
                k = firstIntersection(oRays[index], world);
            }
            else{
                k = firstIntersection(pRays[index], world);
            }

            /*if (i > 320 && i < 326 && j > 240 && j < 246) {
                std::cout << "i: " << i << std::endl;
                std::cout << "j: " << j << std::endl;
                std::cout << "k: " << k << std::endl;
                //std::cout << "i: " << i << std::endl;

            }*/

            // Part 3c
            if (k == -1) {  // Set the color of the pixel to the background color.
                color = make_colour(255*c[0], 255*c[1], 255*c[2]);
            }
            else {  // Set the color of the pixel to the color of the first sphere intersected by the ray.
                color = make_colour(world[k].color.r*255, world[k].color.g*255, world[k].color.b*255);
                //std::cout << "Pretty bird!  " << k << std::endl;

            }

            image.set_pixel(i, j, color);
        }
    }


}

int main(int argc, char** argv) {

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(640, 480);

    // build world
    std::vector<Sphere> world = {
        Sphere(glm::vec3( 0,  0,  1), 1, glm::vec3(  1,   1,   0)),  // yellow sphere
        Sphere(glm::vec3(-1, -1,  3), 2, glm::vec3(  0, 0.5,   0)),  // dark green sphere
        Sphere(glm::vec3( 1,  1,  4), 2, glm::vec3(  1, 0.5,   0)),  // orange sphere
        Sphere(glm::vec3( 2,  2,  6), 3, glm::vec3(  1,   0,   0)),  // red sphere
        Sphere(glm::vec3(-2, -2,  6), 3, glm::vec3(  1,   1,   1))   // white sphere
    };

    // render the world
    render(image, world);

    //image.save_image("ray-traced.bmp");
    if (typeOfView == 0) {
        image.save_image("orthographic.bmp");
    }
    else{
        image.save_image("perspective.bmp");
    }

    std::cout << "Success!" << std::endl;
}
