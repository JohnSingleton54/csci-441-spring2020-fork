// John M. Singleton
// CSCI 441 - Computer Graphics
// Lab 9
// due by 4:30 PM on T 4/21/2020

// References:
// 1. https://www.geeksforgeeks.org/virtual-function-cpp/
// 2. http://www.cplusplus.com/doc/tutorial/files/
// 3. https://gist.github.com/toboqus/def6a6915e4abd66e922    (for a basic binary tree in C++)


#include <iostream>
#include <fstream> // (JMS) I added this.

#include <glm/glm.hpp>

#include <bitmap/bitmap_image.hpp>

#include "camera.h"
#include "hit.h"
#include "intersector.h"
#include "light.h"
#include "ray.h"
#include "renderer.h"
#include "shape.h"
#include "timer.h"

// https://stackoverflow.com/questions/15857839/c-time-stamp-identifier-not-found
// ...Windows machine
#include <time.h>

std::vector<Triangle> bbox;

struct bvhNode{
    std::vector<Triangle> bbox;
    int axis;
    struct bvhNode *left;
    struct bvhNode *right;

    bvhNode(const std::vector<Triangle> bbox, const int axis) : 
    bbox(bbox), axis(axis), left(NULL), right(NULL){}
};

class BvhTree{
public:
   BvhTree();
   ~BvhTree();

   //void insert(std::vector<Triangle> bbox);
   void destroyTree();
   void create(const std::vector<Triangle>& bbox, const World& world);
   //void insert(std::vector<Triangle> bbox)
   void inorderPrint();

private:
    void destroyTree(bvhNode *leaf);
    //void insert(std::vector<Triangle> bbox, bvhNode *leaf);
    void inorderPrint(bvhNode *leaf);

   bvhNode *root;
};

BvhTree::BvhTree(){
    root = NULL;
}

BvhTree::~BvhTree(){
    destroyTree();
}

void BvhTree::destroyTree(bvhNode *leaf){
    if (leaf != NULL){
        destroyTree(leaf->left);
        destroyTree(leaf->right);
        delete leaf;
    }
}

void BvhTree::destroyTree(){
    destroyTree(root);
}

void BvhTree::create(const std::vector<Triangle>& bbox, const World& world){
    root = new bvhNode(bbox, 0);

    int N = world.shapes().size();
    for (auto surface : world.shapes()) {
        std::cout << "id: " << surface->id() << std::endl;
    }

}

/*
void bvhTree::insert(int key, bvhNode *leaf){
    if (key < leaf->value){
        if (leaf->left != NULL){
            insert(key, leaf->left);
        }
        else{
            leaf->left = new bvhNode;
            leaf->left->value = key;
            leaf->left->left = NULL;
            leaf->left->right = NULL;
        }
    }
    else if (key >= leaf->value){
        if (leaf->right != NULL){
            insert(key, leaf->right);
        }
        else{
            leaf->right - new bvhNode;
            leaf->right->value = key;
            leaf->right->right = NULL;
            leaf->right->left = NULL;
        }
    }
}

void bvhTree::insert(int key){
    if (root != NULL){
        insert(key, root);
    }
    else{
        root = new bvhNode;
        root->value = key;
        root->left = NULL;
        root->right = NULL;
    }
}
*/

void BvhTree::inorderPrint(){
    inorderPrint(root);
    std::cout << "\n";
}

void BvhTree::inorderPrint(bvhNode *leaf){
    if (leaf != NULL){
        inorderPrint(leaf->left);
        std::cout << leaf->axis << ",";
        inorderPrint(leaf->right);
    }
}

// BruteForceIntersector is a derived class. Intersector is the base class. 
class BruteForceIntersector : public Intersector {
public:
    Hit find_first_intersection(const World& world, const Ray& ray) {
        Hit hit(ray);
        for (auto surface : world.shapes()) {
            double cur_t = surface->intersect(ray);

            if (cur_t < hit.t()) {
                hit.update(surface, cur_t);
            }
        }
        return hit;
    }
};


// Part 2 - Ray/box predicate, implementation based on FoCG C12 and lecture notes
bool rayIntersectBox(const Ray& r, const std::vector<Triangle>& box){
    glm::vec3 e = r.origin;
    glm::vec3 d = r.direction;

    double xmin, xmax, ymin, ymax, zmin, zmax;
    xmin = box[0].getA().x;
    ymin = box[0].getA().y;
    zmin = box[0].getA().z;
    xmax = box[2].getC().x;
    ymax = box[2].getC().y;
    zmax = box[2].getC().z;

    double txmin, txmax, tymin, tymax, tzmin, tzmax;

    double a;
    a = 1 / d.x;
    if (a >= 0){
        txmin = (xmin - e.x) / d.x;
        txmax = (xmax - e.x) / d.x;
    }
    else{
        txmin = (xmax - e.x) / d.x;
        txmax = (xmin - e.x) / d.x;
    }
    a = 1 / d.y;
    if (a >= 0){
        tymin = (ymin - e.y) / d.y;
        tymax = (ymax - e.y) / d.y;
    }
    else{
        tymin = (ymax - e.y) / d.y;
        tymax = (ymin - e.y) / d.y;
    }
    a = 1 / d.z;
    if (a >= 0){
        tzmin = (zmin - e.z) / d.z;
        tzmax = (zmax - e.z) / d.z;
    }
    else{
        tzmin = (zmax - e.z) / d.z;
        tzmax = (zmin - e.z) / d.z;
    }

    if      (txmin > tymax || tymin > txmax){
        return false;
    }
    else if (tymin > tzmax || tzmin > tymax){
        return false;
    }
    else if (tzmin > txmax || txmin > tzmax){
        return false;
    }
    else{
        return true;
    }
    
}


// a single bounding box
class MySlickIntersector1 : public Intersector {
public:
    Hit find_first_intersection(const World& world, const Ray& ray) {
        Hit hit(ray);


        // TODO: accelerate finding intersections with a spatial data structure.
        if (rayIntersectBox(ray, bbox) ){
            for (auto surface : world.shapes()) {
                double cur_t = surface->intersect(ray);

                if (cur_t < hit.t()) {
                    hit.update(surface, cur_t);
                }
            }            
            //std::cout << "We're the turtles." << std::endl;
        }


        return hit;
    }
};

// Incomplete!
class MySlickIntersector2 : public Intersector {
public:
    Hit find_first_intersection(const World& world, const Ray& ray) {
        Hit hit(ray);


        // TODO: accelerate finding intersections with a spatial data structure.
        if (rayIntersectBox(ray, bbox) ){
            for (auto surface : world.shapes()) {
                double cur_t = surface->intersect(ray);

                if (cur_t < hit.t()) {
                    hit.update(surface, cur_t);
                }
            }            
            //std::cout << "We're the turtles." << std::endl;
        }


        return hit;
    }
};


double rand_val() {
    static bool init = true;
    if (init) {
        srand(time(NULL));
        init = false;
    }
    return ((double) rand() / (RAND_MAX));
}

glm::vec3 rand_color() {
    return glm::vec3(rand_val(),rand_val(),rand_val());
}


std::vector<Triangle> random_box() {
    float  x = (rand_val() * 8) - 4;
    float  y = (rand_val() * 8) - 4;
    float  z = rand_val() * 5;
    float scale = rand_val() * 2;
    return Obj::make_box(glm::vec3(x, y, z), scale, rand_color());
}


std::vector<Triangle> boundingBox(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax){
    return {
        Triangle(glm::vec3(xmin, ymin, zmin), glm::vec3(xmax, ymin, zmin), glm::vec3(xmax, ymax, zmin)),
        Triangle(glm::vec3(xmax, ymax, zmin), glm::vec3(xmin, ymax, zmin), glm::vec3(xmin, ymin, zmin)),

        Triangle(glm::vec3(xmin, ymin, zmax), glm::vec3(xmax, ymin, zmax), glm::vec3(xmax, ymax, zmax)),
        Triangle(glm::vec3(xmax, ymax, zmax), glm::vec3(xmin, ymax, zmax), glm::vec3(xmin, ymin, zmax)),

        Triangle(glm::vec3(xmin, ymax, zmax), glm::vec3(xmin, ymax, zmin), glm::vec3(xmin, ymin, zmin)),
        Triangle(glm::vec3(xmin, ymin, zmin), glm::vec3(xmin, ymin, zmax), glm::vec3(xmin, ymax, xmax)),

        Triangle(glm::vec3(xmax, ymax, zmax), glm::vec3(xmax, ymax, zmin), glm::vec3(xmax, ymin, zmin)),
        Triangle(glm::vec3(xmax, ymin, zmin), glm::vec3(xmax, ymin, zmax), glm::vec3(xmax, ymax, zmax)),

        Triangle(glm::vec3(xmin, ymin, zmin), glm::vec3(xmax, ymin, zmin), glm::vec3(xmax, ymin, zmax)),
        Triangle(glm::vec3(xmax, ymin, zmax), glm::vec3(xmin, ymin, zmax), glm::vec3(xmin, ymin, zmin)),

        Triangle(glm::vec3(xmin, ymax, zmin), glm::vec3(xmax, ymax, zmin), glm::vec3(xmax, ymax, zmax)),
        Triangle(glm::vec3(xmax, ymax, zmax), glm::vec3(xmin, ymax, zmax), glm::vec3(xmin, ymax, zmin))
    };
}


/*
// Part 2 - Ray/box predicate, a very slow implementation
bool rayIntersectBox(const Ray& r, const std::vector<Triangle>& box){
    bool iFlag = false;
    double t;

    for (int i = 0; i < box.size(); i++){
        t = box[i].intersect(r, 0.0, std::numeric_limits<double>::infinity());
        if (t > 0.0 && t < std::numeric_limits<double>::infinity()){
            iFlag = true;
            break;
        }
    }
    return iFlag;
}
*/


int main(int argc, char** argv) {
    /*
    std::vector<Triangle> testBox1 = Obj::make_box(glm::vec3(0, 0, 0), 1, glm::vec3(1, 0, 0));
    std::vector<Triangle> testBox2 = Obj::make_box(glm::vec3(1, 0, 0), 1, glm::vec3(0, 1, 0));
    Ray testRay1;
    glm::vec3 origin = glm::vec3(0, 0, -2);
    glm::vec3 direction = glm::vec3(0, 0, 1);
    testRay1.origin = origin;
    testRay1.direction = direction;

    if (rayIntersectBox(testRay1, testBox1)){
        std::cout << "A I first produced my pistol" << std::endl;
    }
    else{
        std::cout << "A And then produced my rapier" << std::endl;
    }

    if (rayIntersectBox(testRay1, testBox2)){
        std::cout << "B I first produced my pistol" << std::endl;
    }
    else{
        std::cout << "B And then produced my rapier" << std::endl;
    }
    */

    /*bvhTree *tree = new bvhTree();
    tree->insert(10);
    tree->insert(6);

    tree->inorderPrint();

    delete tree;*/



    //std::ofstream myfile;
    //myfile.open("../Part1.txt");
    //myfile.open("../SingleBoundingBox.txt");

    // set the number of boxes
    //for (int NUM_BOXES = 1; NUM_BOXES <= 25; NUM_BOXES++){
    int NUM_BOXES = 3;

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(640, 480);

    // setup the camera
    float dist_to_origin = 5;
    Camera camera(
            glm::vec3(0, 0, -dist_to_origin),   // eye
            glm::vec3(0, 0, 0),                 // target
            glm::vec3(0, 1, 0),                 // up
            glm::vec2(-5, -5),                  // viewport min
            glm::vec2(5, 5),                    // viewport max
            dist_to_origin,                     // distance from eye to view plane
            glm::vec3(.3, .6, .8)               // background color
    );

    // setup lights
    // see http://wiki.ogre3d.org/tiki-index.php?page=-Point+Light+Attenuation
    // for good attenuation value.
    // I found the values at 7 to be nice looking
    PointLight l1(glm::vec3(1, 1, 1), glm::vec3(3, -3, 0), 1.0, .7, 0.18);
    DirectionalLight l2(glm::vec3(.5, .5, .5), glm::vec3(-5, 4, -1));
    Lights lights = { &l1, &l2 };

    // setup world
    World world;

    // add the light
    world.append(Sphere(l1.position(), .25, glm::vec3(1,1,1)));

    // and the spheres
    world.append(Sphere(glm::vec3(1, 1, 1), 1, rand_color()));
    world.append(Sphere(glm::vec3(2, 2, 4), 2, rand_color()));
    world.append(Sphere(glm::vec3(3, 3, 6), 3, rand_color()));

    // I hard-coded these values by looking at the four spheres above.
    double xmin = 0;
    double xmax = 6;
    double ymin = -3.25;
    double ymax = 6;
    double zmin = -0.25;
    double zmax = 9;

    std::vector<Triangle> box;

    // and add some boxes and prep world for rendering
    for (int i = 0 ; i < NUM_BOXES ; ++i) {
        box = random_box();
        if (box[0].getA().x < xmin) { xmin = box[0].getA().x; }
        if (box[0].getA().y < ymin) { ymin = box[0].getA().y; }
        if (box[0].getA().z < zmin) { zmin = box[0].getA().z; }
        if (box[2].getC().x > xmax) { xmax = box[2].getC().x; }
        if (box[2].getC().y > ymax) { ymax = box[2].getC().y; }
        if (box[2].getC().z > zmax) { zmax = box[2].getC().z; }

        world.append(box);
    }
    world.lock();

    bbox = boundingBox(xmin, xmax, ymin, ymax, zmin, zmax);

    //std::cout << "xmin: " << xmin << ", xmax: " << xmax << ", ymin: " << ymin << ", ymax: " << ymax << ", zmin: " << zmin << ", zmax: " << zmax << std::endl;


    // Build a tree for a bounding volume hierarchy. See p. 305 of FoCG.
    // Each of the twelve triangles of a box are treated as a separate object here.
    int N = world.shapes().size();

    BvhTree *bvhTree = new BvhTree();
    //bvhNode *root = new bvhNode(bbox, 0);
    //std::cout << "Did I create an instance of bvhNode? " << root->bbox[0].getA().x << std::endl;
    bvhTree->create(bbox, world);

    bvhTree->inorderPrint();

    std::cout << "N: " << N << std::endl;

    delete bvhTree;


    // create the intersector
    //BruteForceIntersector intersector;
    MySlickIntersector1 intersector;

    // and setup the renderer
    Renderer renderer(&intersector);

    // render
    Timer timer;
    timer.start();
    renderer.render(image, camera, lights, world);
    timer.stop();

    image.save_image("ray-traced.bmp");
    //std::cout << "Rendered in " <<  timer.total() << " milliseconds" << std::endl;
    std::cout << NUM_BOXES << " Rendered in " <<  timer.total() << " milliseconds" << std::endl;
    //myfile << NUM_BOXES << " " << timer.total() << "\n";
    //} // end for loop
    //myfile.close();

    return 0;
}
