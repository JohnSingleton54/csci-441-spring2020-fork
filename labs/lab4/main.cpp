// John M. Singleton
// CSCI 441 - Computer Graphics
// HW4
// due: T 2/18/2020 by 4:30 PM

// incomplete: I believe matrix4.h is complete and that my issue with Step 1 most likely involves my
// calculations of the vectors u, v, w

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix4.h>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

int mode = 0;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window, Shader &shader) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

// ***** ***** ***** *****
void keyCallback( GLFWwindow *window, int key, int scancode, int action, int mods );
// ***** ***** ***** *****

int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Lab 4", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

// ***** ***** ***** *****
    glfwSetKeyCallback( window, keyCallback );
// ***** ***** ***** *****

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* init the model */
    float vertices[] = {
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,

        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,

        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f
    };

    // copy vertex data
    GLuint VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // describe vertex layout
    GLuint VAO;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    // setup the textures
    shader.use();

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    Matrix4 model; // the model matrix
    Matrix4 view; // the view matrix
    Matrix4 proj; // the projection matrix

    float gy = 0.0f;
    float gz = 20.0f;
    float w[] = {0.0f, 0.0f, 0.0f};
    float up[] = {0.0f, 1.0f, 0.0f}; // define "up"
    float u[] = {0.0f, 0.0f, 0.0f};
    float v[] = {0.0f, 0.0f, 0.0f};

    std::cout << "Hello Tina!" << std::endl;

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(window, shader);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


        // activate shader
        shader.use();

        // ***** ***** ***** *****

        switch(mode){
            case 1:{ // w
                gy -= 1;
                w[1] = -gy / sqrt(gy*gy + gz*gz);
                //std::cout << w[1] << std::endl;
                w[2] = -gz / sqrt(gy*gy + gz*gz);
                u[1] = gz / sqrt(gy*gy + gz*gz);
                u[2] = gy / sqrt(gy*gy + gz*gz);
                u[1] = gz / sqrt(gy*gy + gz*gz);
                u[2] = gy / sqrt(gy*gy + gz*gz);

                v[0] = w[1]*u[2] - w[2]*u[1];
                v[1] = w[2]*u[0] - w[0]*u[2];
                v[0] = w[1]*u[2] - w[2]*u[1];
                v[1] = w[2]*u[0] - w[0]*u[2];
                //std::cout << v[1] << std::endl;
                v[2] = w[0]*u[1] - w[1]*u[0];
                Matrix4 Rinv;
                Rinv.set(0, u[0]);
                Rinv.set(1, u[1]);
                Rinv.set(2, u[2]);
                Rinv.set(4, v[0]);
                Rinv.set(5, v[1]);
                Rinv.set(6, v[2]);
                Rinv.set(8, w[0]);
                Rinv.set(9, w[1]);
                Rinv.set(10, w[2]);
                std::cout << Rinv.to_string() << std::endl;
                //std::cout << Rinv.values[5] << std::endl;
                Matrix4 Tinv;
                //Tinv.set(3, );
                Tinv.set(7, -w[1]); //gy);
                Tinv.set(11, -w[2]); //gz);
                view = Rinv.multiply(Tinv);
                std::cout << view.to_string() << std::endl;
                //view.translate(0.0f, -0.33f, 0.0f);
                mode = 0;
                break;
            }
        }

        //view.translate(0.0f, 0.33f, -0.5f);
        //model.rotate_z(3.14f / 4.0f);

        //model.translate(0, 0.5, 0);

        int vMatLocation = glGetUniformLocation(shader.id(), "vMat");

        glUniformMatrix4fv(vMatLocation, 1, GL_TRUE, view.values); // JMS: switched to GL_TRUE, which seems like an improvement(?)

        int mMatLocation = glGetUniformLocation(shader.id(), "mMat");

        glUniformMatrix4fv(mMatLocation, 1, GL_FALSE, model.values);

        int pMatLocation = glGetUniformLocation(shader.id(), "pMat");

        glUniformMatrix4fv(pMatLocation, 1, GL_FALSE, proj.values);


        // ***** ***** ***** *****

        // render the cube
        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(vertices));

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}

void keyCallback( GLFWwindow *window, int key, int scancode, int action, int mods ){
    if( key == GLFW_KEY_W && action == GLFW_RELEASE ){
        mode = 1;
    }
}