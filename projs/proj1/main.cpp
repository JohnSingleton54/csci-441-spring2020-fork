// John M. Singleton and YaLan (Tina) Yin
// CSCI 441 - Computer Graphics
// Project 1
// due: T 4/7/2020 by 4:30 PM

// References:

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix4.h>
#include <csci441/matrix3.h>
#include <csci441/vector4.h>
#include <csci441/uniform.h>


#include "shape.h"
#include "model.h"
#include "camera.h"
#include "renderer.h"

#include <vector>

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;

int mode = 1;
int maze_switch = 1;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}

Matrix4 initializeModel2(const Matrix4& model, GLFWwindow *window) {
    Matrix4 trans;

    trans.translate(-0.7, -1.0, 0);

    return trans * model;        
}

Matrix4 rotateMaze(const Matrix4& model, GLFWwindow *window, int theta) {
    Matrix4 trans;

    trans.rotate_x(theta);

    return trans * model;        
}

Matrix4 processModel1(const Matrix4& model, GLFWwindow *window) {
    Matrix4 trans;

    const float ROT = 1;
    const float SCALE = .05;
    const float TRANS = .01;

    if (mode == 0){
        // ROTATE
        if      (isPressed(window, GLFW_KEY_U)) { trans.rotate_x(-ROT); }
        else if (isPressed(window, GLFW_KEY_I)) { trans.rotate_x(ROT); }
        else if (isPressed(window, GLFW_KEY_O)) { trans.rotate_y(-ROT); }
        else if (isPressed(window, GLFW_KEY_P)) { trans.rotate_y(ROT); }
        else if (isPressed(window, '[')) { trans.rotate_z(-ROT); }
        else if (isPressed(window, ']')) { trans.rotate_z(ROT); }

    }
        return trans * model;        
}

Matrix4 processModel2(const Matrix4& model, GLFWwindow *window) {
    Matrix4 trans;

    const float ROT = 1;
    const float SCALE = .05;
    const float TRANS = .01;

    if (mode == 0){
        // ROTATE
        if      (isPressed(window, GLFW_KEY_U)) { trans.rotate_x(-ROT); }
        else if (isPressed(window, GLFW_KEY_I)) { trans.rotate_x(ROT); }
        else if (isPressed(window, GLFW_KEY_O)) { trans.rotate_y(-ROT); }
        else if (isPressed(window, GLFW_KEY_P)) { trans.rotate_y(ROT); }
        else if (isPressed(window, '[')) { trans.rotate_z(-ROT); }
        else if (isPressed(window, ']')) { trans.rotate_z(ROT); }
        // SCALE
        else if (isPressed(window, '-')) { trans.scale(1-SCALE, 1-SCALE, 1-SCALE); }
        else if (isPressed(window, '=')) { trans.scale(1+SCALE, 1+SCALE, 1+SCALE); }
        // TRANSLATE
        else if (isPressed(window, GLFW_KEY_UP)) { trans.translate(0, TRANS, 0); }
        else if (isPressed(window, GLFW_KEY_DOWN)) { trans.translate(0, -TRANS, 0); }
        else if (isPressed(window, GLFW_KEY_LEFT)) { trans.translate(-TRANS, 0, 0); }
        else if (isPressed(window, GLFW_KEY_RIGHT)) { trans.translate(TRANS, 0, 0); }
    }
    else if (mode == 1){
        // TRANSLATE
        if (isPressed(window, GLFW_KEY_UP)) { trans.translate(0, TRANS, 0); }
        else if (isPressed(window, GLFW_KEY_DOWN)) { trans.translate(0, -TRANS, 0); }
        else if (isPressed(window, GLFW_KEY_LEFT)) { trans.translate(-TRANS, 0, 0); }
        else if (isPressed(window, GLFW_KEY_RIGHT)) { trans.translate(TRANS, 0, 0); }
    }
        return trans * model;        
}

bool isSpaceEvent(GLFWwindow *window) {
    static bool pressed = false;

    bool trigger = false;
    if (isPressed(window, GLFW_KEY_SPACE)) {
        pressed = true;
    } else if (pressed && isReleased(window, GLFW_KEY_SPACE)) {
        pressed = false;
        trigger = true;
    }
    return trigger;
}

void processInput1(Matrix4& model, GLFWwindow *window) {
    if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    } else if (isSpaceEvent(window)) {
        std::cout << "Samurai come!" << std::endl;        
        if      (mode == 0){
            mode = 1;
            model = rotateMaze(model, window, 90);
            std::cout << "Hangar 18. I know too much." << std::endl;
        }
        else if (mode == 1){
            mode = 0;
            model = rotateMaze(model, window, -90);
        }
    }

    model = processModel1(model, window);
}

void processInput2(Matrix4& model, GLFWwindow *window) {
    if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    } else if (isSpaceEvent(window)) {
        std::cout << "Samurai come!" << std::endl;        
        if      (mode == 0){
            mode = 1;
            model = rotateMaze(model, window, 90);
            std::cout << "Hangar 18. I know too much." << std::endl;
        }
        else if (mode == 1){
            mode = 0;
            model = rotateMaze(model, window, -90);
        }
    }

    model = processModel2(model, window);
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void){ 
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "CSCI441-lab", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    // Set up the cameras.
    Matrix4 projection, orthographic;
    Camera camera, bird, cameraToUse;

    projection.perspective(45, 1, .1, 20);  // original: projection.perspective(45, 1, 0.01, 20);

    camera.projection = projection;
    camera.eye    = Vector4( 0,  0,  3);  // original: camera.eye = Vector4(0, 0, 3);
    camera.origin = Vector4( 0,  0,  0);
    camera.up     = Vector4( 0,  1,  0);  // original: camera.up = Vector4(0, 1, 0);

    orthographic.ortho(-2, 2, -2, 2, -10, 10);
    bird.projection = orthographic;
    bird.eye      = Vector4( -0.0223607,  0.0223607, 0.999);
    bird.origin   = Vector4( 0,  0,  0);
    bird.up       = Vector4( 0,  1,  0);
    //bird.up       = Vector4( 0,  0.9, 0.435890);

    cameraToUse = camera;

    //Model obj1(Maze0().coords, Shader("../vert.glsl", "../frag.glsl"));
    //if (maze_switch == 1){
        Maze1 maze1(1.0, 1.0, 1.0);
        maze1.load_obj("../models/maze.obj");
        Model obj1(maze1.coords, Shader("../vert.glsl", "../frag.glsl"));
    //}

    Model obj2(Sphere(100, 0.033f, 1.0f, 0.0f, 0.0f).coords, Shader("../vert.glsl", "../frag.glsl"));

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    // Initialize the character's location to the Start of the maze.
    obj2.model = initializeModel2(obj2.model, window);

    // Create a renderer.
    Renderer renderer;

    // set the light position
    Vector4 lightPos(4.0f, 4.0f, 4.0f); // original: Vector4 lightPos(3.75f, 3.75f, 4.0f);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // Process input.
    	processInput1(obj1.model, window);
        processInput2(obj2.model, window);

        /* Render here */
        // glClearColor(0.2f, 0.3f, 0.3f, 1.0f); //   Dave's nice background color
        glClearColor(0.5f, 1.0f, 0.5f, 1.0f);    // a light green background color
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        if      (mode == 0){
            cameraToUse = camera; // camera view
        }
        else if (mode == 1){
            cameraToUse = bird; // bird's eye view
        }

        renderer.render(cameraToUse, obj1, lightPos);
        renderer.render(cameraToUse, obj2, lightPos);

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;        
}
