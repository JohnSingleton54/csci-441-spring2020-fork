#ifndef _CSCI441_MODEL_H_
#define _CSCI441_MODEL_H_

#include <vector>
#include <iostream>

class Model {

public:
    bool isSmooth;
    GLuint vbo;
    GLuint vao;
    Shader shader;
    Matrix4 model;
    int size;

    GLuint ibo;
    int num_vertices;
    std::vector<GLuint> indices;

    std::vector<float> flat_coords;
    std::vector<float> indexed_coords;
    struct Vertex{
        float x, y, z, r, g, b, nx, ny, nz;
    };
    std::vector<Vertex> V;
    float eps = 0.000001f;
    int new_size;


    template <typename Coords>
    Model(const Coords& coords, const Shader& shader_in) : shader(shader_in) {
        isSmooth = 1;
        size = coords.size()*sizeof(float);

        num_vertices = coords.size() / 9;

        for (int i = 0; i < coords.size(); i++){
            flat_coords.push_back(coords.at(i));
        }

        for (int i = 0; i < num_vertices; i+=3){
            Vector4 a = Vector4(coords.at(9*i     ), coords.at(9*i +  1), coords.at(9*i +  2), 0.0f);
            Vector4 b = Vector4(coords.at(9*i +  9), coords.at(9*i + 10), coords.at(9*i + 11), 0.0f);
            Vector4 c = Vector4(coords.at(9*i + 18), coords.at(9*i + 19), coords.at(9*i + 20), 0.0f);

            Vector4 bminusa = b - a;
            Vector4 cminusa = c - a;

            Vector4 nv;
            if (i % 6 == 0){
                nv = ( bminusa.cross(cminusa) ).normalized();
            }
            else{
                nv = ( cminusa.cross(bminusa) ).normalized();
            }

            flat_coords.at(9*i +  6) = nv.values[0];
            flat_coords.at(9*i +  7) = nv.values[1];
            flat_coords.at(9*i +  8) = nv.values[2];
            flat_coords.at(9*i + 15) = nv.values[0];
            flat_coords.at(9*i + 16) = nv.values[1];
            flat_coords.at(9*i + 17) = nv.values[2];
            flat_coords.at(9*i + 24) = nv.values[0];
            flat_coords.at(9*i + 25) = nv.values[1];
            flat_coords.at(9*i + 26) = nv.values[2];
        }



        for (int i = 0; i < num_vertices; i++){
            indices.push_back(i);
        }

        for (int i = 0; i < coords.size(); i++){
            indexed_coords.push_back(flat_coords.at(i));
        }

        if (isSmooth){

        indexed_coords.clear();

        // The vector count will grow in parallel with the vector V.
        // count[i] is the number of times that V[i] was encountered.
        // * I don't think count is needed. *
        std::vector<int> count;


        for (int i = 0; i < num_vertices; i++){
            Vertex v;
            v.x  = flat_coords.at(9*i    );
            v.y  = flat_coords.at(9*i + 1);
            v.z  = flat_coords.at(9*i + 2);
            v.r  = flat_coords.at(9*i + 3);
            v.g  = flat_coords.at(9*i + 4);
            v.b  = flat_coords.at(9*i + 5);
            v.nx = flat_coords.at(9*i + 6);
            v.ny = flat_coords.at(9*i + 7);
            v.nz = flat_coords.at(9*i + 8);

            if (i == 0){ // the first vertex is new
                V.push_back(v);
                count.push_back(1);
                indexed_coords.push_back(v.x);
                indexed_coords.push_back(v.y);              
                indexed_coords.push_back(v.z);
                indexed_coords.push_back(v.r);
                indexed_coords.push_back(v.g);
                indexed_coords.push_back(v.b);
                indexed_coords.push_back(v.nx);
                indexed_coords.push_back(v.ny);
                indexed_coords.push_back(v.nz);               
            }

            else {
                int j = 0;
                int idx = -1;

                while (j < V.size() && idx == -1){

                    Vertex w = V.at(j);
                    if (fabs(w.x - v.x) < eps &&
                        fabs(w.y - v.y) < eps &&
                        fabs(w.z - v.z) < eps   ){ // if vertices w and v are identical
                        idx = j;
                        indices.at(i) = (GLuint)idx;

                        w.nx += v.nx;
                        w.ny += v.ny;
                        w.nz += v.nz;
                        count.at(j) +=1;
                    }

                    j++;

                }

                if (idx == -1){ // if the vertex is new
                    indices.at(i) = (GLuint)V.size();
                    V.push_back(v);
                    count.push_back(1);
                    indexed_coords.push_back(v.x);
                    indexed_coords.push_back(v.y);              
                    indexed_coords.push_back(v.z);
                    indexed_coords.push_back(v.r);
                    indexed_coords.push_back(v.g);
                    indexed_coords.push_back(v.b);
                    indexed_coords.push_back(v.nx);
                    indexed_coords.push_back(v.ny);
                    indexed_coords.push_back(v.nz);
                }
            } // end else
        } // end for loop

        // calculate the normal vectors for smooth shading:
        for (int i = 0; i < V.size(); i++){
            Vector4 v = (Vector4(indexed_coords.at(9*i + 6), indexed_coords.at(9*i + 7), indexed_coords.at(9*i + 8), 0.0f));
            v = v.normalized();
            indexed_coords.at(9*i + 6) = v.values[0];
            indexed_coords.at(9*i + 7) = v.values[1];
            indexed_coords.at(9*i + 8) = v.values[2];
        }

        } // end if (isSmooth)

        new_size = indexed_coords.size()*sizeof(float);

        glGenVertexArrays(1, &vao);
        glGenBuffers(1, &vbo);
        glGenBuffers(1, &ibo);

        glBindVertexArray(vao);

        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, new_size, indexed_coords.data(), GL_STATIC_DRAW);
        //glBufferData(GL_ARRAY_BUFFER, size, coords.data(), GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), indices.data(), GL_STATIC_DRAW); // &indices[0] could be replaced with indices.data()

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
                (void*)(0*sizeof(float)));
        glEnableVertexAttribArray(0);

        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
                (void*)(3*sizeof(float)));
        glEnableVertexAttribArray(1);

        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
                (void*)(6*sizeof(float)));
        glEnableVertexAttribArray(2);
    }
};

#endif
