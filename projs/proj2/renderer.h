#ifndef _CSCI441_RENDERER_H_
#define _CSCI441_RENDERER_H_

#include "camera.h"
#include "hit.h"
#include "light.h"
#include "ray.h"
#include "intersector.h"

class Renderer {

    //double ymin = 0.0; double ymax = 0.0;

    Intersector* _intersector;

    int clamp255(float v) {
        return std::max(0.0f, std::min(255.0f, 255.0f*v));
    }


    rgb_t to_color(const glm::vec3 c) {
        return make_colour(clamp255(c.x), clamp255(c.y), clamp255(c.z));
    }

    glm::vec3 phong(const Hit& hit, const Light* light, const glm::vec3& eye, bool shadowFlag){
        float ambientStrength = 0.2;
        float specularStrength = 0.5;
        float shinyness = 2;

        glm::vec3 pos = hit.position();
        glm::vec3 normal = hit.normal();

        glm::vec3 light_dir = glm::normalize(light->direction(pos));

        float ambient = ambientStrength;

        float diffuse = 0;
        float specular = 0;
        if (!shadowFlag){
            //std::cout << "The window burns to light the way back home" << std::endl;
            diffuse = glm::max(glm::dot(normal, light_dir), 0.0f);

            glm::vec3 view_dir = glm::normalize(eye - pos);
            glm::vec3 reflectDir = glm::reflect(-light_dir, normal);
            specular = specularStrength *
                pow(std::fmax(glm::dot(view_dir, reflectDir), 0.0), shinyness);
        }

        glm::vec3 light_color  =
            (ambient+diffuse+specular)
            * light->attenuation(pos)
            * light->color();

        return light_color*hit.color();
    }

    bool intersectShadowRay(const Light* light, const World& world, const Hit& hit, glm::vec3 p){
        glm::vec3 dir = glm::normalize(light->direction(p));
        Ray rayToLight;
        rayToLight.origin = p;
        rayToLight.direction = dir;

        int id = hit.shapeId();

        bool inShadow = false;

        Hit hitLight(rayToLight);
        double l_t = world.shapes().at(0)->intersect(rayToLight);
        hitLight.update(world.shapes().at(0), l_t);
        double eps = 0.01;
        for (auto surface : world.shapes()){
            double cur_t = surface->intersect(rayToLight);
            if (surface->id() != hit.shapeId() &&  // *** This assumes that a shape can't cast a
                                                   // shadow on itself. ***
              cur_t > eps &&  // redundant?
              cur_t < (l_t - eps) &&
              glm::dot(hit.normal(), -dir) < 0){
                //if (dir.y < ymin) { ymin = dir.y; }
                //if (dir.y > ymax) { ymax = dir.y; }
                //std::cout << "dir.x: " << dir.x <<", dir.y: " << dir.y << ", dir.z: " << dir.z << std::endl;
                inShadow = true;
                break;
            }
        }

        return inShadow;
    }

    glm::vec3 shade(const Camera& camera, const Lights& lights, const World& world, const Hit& hit){
        glm::vec3 color = camera.background;
        if (hit.is_intersection()) {
            color = glm::vec3(0,0,0);

            glm::vec3 p = hit.position();
            //std::cout << "p.x: " << p.x << ", p.y: " << p.y << ", p.z: " << p.z << std::endl;
            for (auto light : lights) {
                bool shadowFlag = intersectShadowRay(light, world, hit, p);
                color += phong(hit, light, camera.pos, shadowFlag);

                // Feature 4, reflections, is implemented here.
                if (hit.shapeId() == 4){ // the reflective surface(s)
                    glm::vec3 d = glm::normalize(hit.ray().direction);
                    glm::vec3 n = hit.normal();
                    glm::vec3 r = glm::normalize(d - 2*glm::dot(d, n)*n);
                    Ray rayToReflected;
                    rayToReflected.origin = p;
                    rayToReflected.direction = r;

                    bool getsReflection = false;

                    Hit hitToReflected = Hit(rayToReflected);
                    double eps = 0.01;
                    for (auto surface : world.shapes()){
                        double cur_t = surface->intersect(rayToReflected);
                        if (cur_t < hitToReflected.t()){
                            getsReflection = true;
                            hitToReflected.update(surface, cur_t);
                        }
                    }
                    if (getsReflection){
                        for (auto light : lights){
                            bool r_shadowFlag = intersectShadowRay(light, world, hitToReflected, glm::vec3(hit.position()));
                            glm::vec3 c1 = phong(hitToReflected, light, p, r_shadowFlag);
                            c1.r *= c1.r;
                            c1.g *= c1.g;
                            c1.b *= c1.b;
                            color += c1;
                        }
                    }
                }
            }
        }
        return color;
    }

    glm::vec3 render_pixel(
        const Camera& camera,
        const Lights& lights,
        const World& world,
        const Ray& ray
    ) {

        Hit hit = _intersector->find_first_intersection(world, ray);
        return shade(camera, lights, world, hit);
    }

public:

    Renderer(Intersector* intersector) : _intersector(intersector) { }

    void render(
        bitmap_image &image,
        const Camera& camera,
        const Lights& lights,
        const World& world,
        int xi,
        int xf
    ) {

        // The upper half of the image is antialiased.
        // (See FoCG pp. 330+331.)
        for (int y = image.height() / 2; y < image.height(); ++y) {
            for (int x = xi; x < xf; ++x) {
                // if we are rendering a point on the boundary of the image:
                if (x == 0 || x == (image.width() - 1) || y == 0 || y == (image.height() - 1)){
                    Ray ray = camera.make_ray(image.width(), image.height(), x, y);
                    glm::vec3 c = render_pixel(camera, lights, world, ray);
                    image.set_pixel(x, image.height()-y-1, to_color(c));
                }
                // else if we are NOT rendering a point on the boundary of the image:
                else{
                    double cx = 0; double cy = 0; double cz = 0;
                    Ray ray;
                    glm::vec3 c;
                    ray = camera.make_ray(image.width() - 1, image.height() - 1, x, y);
                    c = render_pixel(camera, lights, world, ray);
                    cx = cx + c.x; cy = cy + c.y; cz = cz + c.z;
                    ray = camera.make_ray(image.width() - 1, image.height()    , x, y);
                    c = render_pixel(camera, lights, world, ray);
                    cx = cx + c.x; cy = cy + c.y; cz = cz + c.z;
                    ray = camera.make_ray(image.width() - 1, image.height() + 1, x, y);
                    c = render_pixel(camera, lights, world, ray);
                    cx = cx + c.x; cy = cy + c.y; cz = cz + c.z;
                    ray = camera.make_ray(image.width()    , image.height() - 1, x, y);
                    c = render_pixel(camera, lights, world, ray);
                    cx = cx + c.x; cy = cy + c.y; cz = cz + c.z;
                    ray = camera.make_ray(image.width()    , image.height()    , x, y);
                    c = render_pixel(camera, lights, world, ray);
                    cx = cx + c.x; cy = cy + c.y; cz = cz + c.z;
                    ray = camera.make_ray(image.width()    , image.height() + 1, x, y);
                    c = render_pixel(camera, lights, world, ray);
                    cx = cx + c.x; cy = cy + c.y; cz = cz + c.z;
                    ray = camera.make_ray(image.width() + 1, image.height() - 1, x, y);
                    c = render_pixel(camera, lights, world, ray);
                    cx = cx + c.x; cy = cy + c.y; cz = cz + c.z;
                    ray = camera.make_ray(image.width() + 1, image.height()    , x, y);
                    c = render_pixel(camera, lights, world, ray);
                    cx = cx + c.x; cy = cy + c.y; cz = cz + c.z;
                    ray = camera.make_ray(image.width() + 1, image.height() + 1, x, y);
                    c = render_pixel(camera, lights, world, ray);
                    cx = cx + c.x; cy = cy + c.y; cz = cz + c.z;

                    c = glm::vec3(cx/9, cy/9, cz/9);
                    image.set_pixel(x, image.height()-y-1, to_color(c));
                }
            }
        }

        // The lower half of the image is not antialiased.
        for (int y = 0; y < image.height() / 2; ++y) {
            for (int x = 0; x < image.width(); ++x) {
                Ray ray = camera.make_ray(image.width(), image.height(), x, y);
                glm::vec3 c = render_pixel(camera, lights, world, ray);
                image.set_pixel(x, image.height()-y-1, to_color(c));
            }
        }
        //std::cout << "ymin: " << ymin << "ymax: " << ymax << std::endl;

    }
};

#endif
