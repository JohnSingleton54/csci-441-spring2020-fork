// John M. Singleton
// CSCI 441 - Computer Graphics
// Project 2
// due by 4:00 PM on W 5/6/2020

#include <iostream>

#include <glm/glm.hpp>

#include <bitmap/bitmap_image.hpp>

#include "camera.h"
#include "hit.h"
#include "intersector.h"
#include "light.h"
#include "ray.h"
#include "renderer.h"
#include "shape.h"
#include "timer.h"

#include "time.h"
#include <thread>

// For no threads, set numThreads equal to 1.
// *** currently supported values: 1, 2, 4, 8 ***    TODO: Support any divisor.
int numThreads = 4; // ideally, a divisor of image.width() = 640

class BruteForceIntersector : public Intersector {
public:

    Hit find_first_intersection(const World& world, const Ray& ray) {
        Hit hit(ray);
        for (auto surface : world.shapes()) {
            double cur_t = surface->intersect(ray);

            if (cur_t < hit.t()) {
                hit.update(surface, cur_t);
            }
        }
        return hit;
    }
};

class MySlickIntersector : public Intersector {
public:
    Hit find_first_intersection(const World& world, const Ray& ray) {
        Hit hit(ray);
        // TODO: accelerate finding intersections with a spatial data structure.
        return hit;
    }
};

double rand_val() {
    static bool init = true;
    if (init) {
        srand(time(NULL));
        init = false;
    }
    return ((double) rand() / (RAND_MAX));
}

glm::vec3 rand_color() {
    return glm::vec3(rand_val(),rand_val(),rand_val());
}

std::vector<Triangle> random_box() {
    float  x = (rand_val() * 8) - 4;
    float  y = (rand_val() * 8) - 4;
    float  z = rand_val() * 5;
    float scale = rand_val() * 2;
    return Obj::make_box(glm::vec3(x, y, z), scale, rand_color());
}

void render(Renderer& renderer, bitmap_image& image, const Camera& camera, const Lights& lights, const World& world, int xi, int xf){
    renderer.render(image, camera, lights, world, xi, xf);
}

int main(int argc, char** argv) {

    // set the number of boxes
    //int NUM_BOXES = 1;

    // create an image 640 pixels wide by 480 pixels tall
    // adjust resolution here?
    //bitmap_image image(320, 240);
    //bitmap_image image(640, 480);
    bitmap_image image(1280, 960);

    // setup the camera
    float dist_to_origin = 5;
    Camera camera(
            glm::vec3(0, 0, -dist_to_origin),   // eye
            glm::vec3(0, 0, 0),                 // target
            glm::vec3(0, 1, 0),                 // up
            glm::vec2(-5, -5),                  // viewport min
            glm::vec2(5, 5),                    // viewport max
            dist_to_origin,                     // distance from eye to view plane
            glm::vec3(0.5, 1, 0.5)                 // background color
    );

    // setup lights
    // see http://wiki.ogre3d.org/tiki-index.php?page=-Point+Light+Attenuation
    // for good attenuation value.
    // I found the values at 7 to be nice looking

    // JMS: A cube with a light at each of its corners and at its center.
    double a = 0.015;
    PointLight l1(glm::vec3(.1, .1, .1), glm::vec3(3, 0, 0),     1, .7, 1.8);
    PointLight l2(glm::vec3(.1, .1, .1), glm::vec3(3+a, -a, -a), 1, .7, 1.8);
    PointLight l3(glm::vec3(.1, .1, .1), glm::vec3(3+a, -a, +a), 1, .7, 1.8);
    PointLight l4(glm::vec3(.1, .1, .1), glm::vec3(3+a, +a, -a), 1, .7, 1.8);
    PointLight l5(glm::vec3(.1, .1, .1), glm::vec3(3+a, +a, +a), 1, .7, 1.8);
    PointLight l6(glm::vec3(.1, .1, .1), glm::vec3(3-a, -a, -a), 1, .7, 1.8);
    PointLight l7(glm::vec3(.1, .1, .1), glm::vec3(3-a, -a, +a), 1, .7, 1.8);
    PointLight l8(glm::vec3(.1, .1, .1), glm::vec3(3-a, +a, -a), 1, .7, 1.8);
    PointLight l9(glm::vec3(.1, .1, .1), glm::vec3(3-a, +a, +a), 1, .7, 1.8);

    //DirectionalLight l10(glm::vec3(.5, .5, .5), glm::vec3(-5, 4, -1));
    DirectionalLight l10(glm::vec3(.5, .5, .5), glm::vec3(-5, 4, -8));
    Lights lights = { &l1, &l2, &l3, &l4, &l5, &l6, &l7, &l8, &l9,&l10 };

    // setup world
    World world;

    // add the light
    world.append(Sphere(l1.position(), .25, glm::vec3(1,1,1)));

    // and the spheres
    world.append(Sphere(glm::vec3(-3, 0, 1), 0.9, glm::vec3(1, 0, 0)));
    world.append(Sphere(glm::vec3(0, 0, 0.5), 0.33, glm::vec3(0, 1, 0)));

    world.append(Sphere(glm::vec3(1.5, -3.2, 2.5), 1.75, glm::vec3(0, 0.5, 0)));
    world.append(Sphere(glm::vec3(1.5,  3.2, 2.5), 1.25, glm::vec3(0, 0.5, 0)));

    std::vector<Triangle> box1 = Obj::make_box(glm::vec3(-3.5, -3.2, 2.5), 2, glm::vec3(1, 0, 0));
    world.append(box1);

    std::vector<Triangle> box2 = Obj::make_box(glm::vec3(-3.5,  3.2, 2.5), 2, glm::vec3(1, 0, 0));
    world.append(box2);

    std::vector<Triangle> box3 = Obj::make_box(glm::vec3(2.25, 1.6, 1), .33, glm::vec3(1, 0.9, .34));
    world.append(box3);

    //std::vector<Triangle> box4 = Obj::make_box(glm::vec3(3, 2, 2.5), 3, glm::vec3(1, 0.5, 0));
    //world.append(box4);

/*
    // and add some boxes and prep world for rendering
    for (int i = 0 ; i < NUM_BOXES ; ++i) {
        world.append(random_box());
    }
*/

    world.lock();

    //glm::vec3 p = glm::vec3(-3, 0, 1);
    //glm::vec3 dir = glm::normalize(lights[0]->direction(p));
    //std::cout << "direction from (-3, 0, 1) to (3, 0, 0): " << dir.x << " " << dir.y << " " << dir.z << std::endl;

    //int l1_id = world.shapes().at(0)->id();
    //std::cout << "l1_id:" << l1_id << std::endl;

    //double dp = glm::dot(glm::vec3(1,0,0), glm::vec3(-1,0,0));
    //std::cout << "dp: " << dp << std::endl;

    Timer timer;

    if (numThreads == 1){ // no threads
        // create the intersector
        BruteForceIntersector intersector;
        // and setup the renderer
        Renderer renderer(&intersector);
        // render
        timer.start();

        renderer.render(image, camera, lights, world, 0, image.width());

        timer.stop();
    }

    else if (numThreads == 2){
        BruteForceIntersector intersector1;
        BruteForceIntersector intersector2;

        Renderer renderer1(&intersector1);
        Renderer renderer2(&intersector2);

        timer.start();
        
        int xi = 0; int xf = image.width()/2;
        std::thread th1(render, std::ref(renderer1), std::ref(image), std::ref(camera), std::ref(lights), std::ref(world), xi, xf);
        xi = image.width()/2; xf = image.width();
        std::thread th2(render, std::ref(renderer2), std::ref(image), std::ref(camera), std::ref(lights), std::ref(world), xi, xf);

        th1.join();
        th2.join();

        timer.stop();
    }

    else if (numThreads == 4){
        BruteForceIntersector intersector1;
        BruteForceIntersector intersector2;
        BruteForceIntersector intersector3;
        BruteForceIntersector intersector4;

        Renderer renderer1(&intersector1);
        Renderer renderer2(&intersector2);
        Renderer renderer3(&intersector3);
        Renderer renderer4(&intersector4);

        timer.start();
        
        int xi = 0; int xf = image.width()/4;
        std::thread th1(render, std::ref(renderer1), std::ref(image), std::ref(camera), std::ref(lights), std::ref(world), xi, xf);
        xi = image.width()/4; xf = image.width()/2;
        std::thread th2(render, std::ref(renderer2), std::ref(image), std::ref(camera), std::ref(lights), std::ref(world), xi, xf);
        xi = image.width()/2; xf = image.width()*3/4;
        std::thread th3(render, std::ref(renderer3), std::ref(image), std::ref(camera), std::ref(lights), std::ref(world), xi, xf);
        xi = image.width()*3/4; xf = image.width();
        std::thread th4(render, std::ref(renderer4), std::ref(image), std::ref(camera), std::ref(lights), std::ref(world), xi, xf);

        th1.join();
        th2.join();
        th3.join();
        th4.join();

        timer.stop();
    }

    else if (numThreads == 8){
        BruteForceIntersector intersector1;
        BruteForceIntersector intersector2;
        BruteForceIntersector intersector3;
        BruteForceIntersector intersector4;
        BruteForceIntersector intersector5;
        BruteForceIntersector intersector6;
        BruteForceIntersector intersector7;
        BruteForceIntersector intersector8;


        Renderer renderer1(&intersector1);
        Renderer renderer2(&intersector2);
        Renderer renderer3(&intersector3);
        Renderer renderer4(&intersector4);
        Renderer renderer5(&intersector5);
        Renderer renderer6(&intersector6);
        Renderer renderer7(&intersector7);
        Renderer renderer8(&intersector8);

        timer.start();
        
        int xi = 0; int xf = image.width()*1/8;
        std::thread th1(render, std::ref(renderer1), std::ref(image), std::ref(camera), std::ref(lights), std::ref(world), xi, xf);
        xi = image.width()*1/8; xf = image.width()*2/8;
        std::thread th2(render, std::ref(renderer2), std::ref(image), std::ref(camera), std::ref(lights), std::ref(world), xi, xf);
        xi = image.width()*2/8; xf = image.width()*3/8;
        std::thread th3(render, std::ref(renderer3), std::ref(image), std::ref(camera), std::ref(lights), std::ref(world), xi, xf);
        xi = image.width()*3/8; xf = image.width()*4/8;
        std::thread th4(render, std::ref(renderer4), std::ref(image), std::ref(camera), std::ref(lights), std::ref(world), xi, xf);
        xi = image.width()*4/8; xf = image.width()*5/8;
        std::thread th5(render, std::ref(renderer5), std::ref(image), std::ref(camera), std::ref(lights), std::ref(world), xi, xf);
        xi = image.width()*5/8; xf = image.width()*6/8;
        std::thread th6(render, std::ref(renderer6), std::ref(image), std::ref(camera), std::ref(lights), std::ref(world), xi, xf);
        xi = image.width()*6/8; xf = image.width()*7/8;
        std::thread th7(render, std::ref(renderer7), std::ref(image), std::ref(camera), std::ref(lights), std::ref(world), xi, xf);
        xi = image.width()*7/8; xf = image.width();
        std::thread th8(render, std::ref(renderer8), std::ref(image), std::ref(camera), std::ref(lights), std::ref(world), xi, xf);

        th1.join();
        th2.join();
        th3.join();
        th4.join();
        th5.join();
        th6.join();
        th7.join();
        th8.join();

        timer.stop();
    }

    image.save_image("proj2.bmp");
    std::cout << "Rendered in " <<  timer.total() << " milliseconds" << std::endl;
}
