#ifndef _CSCI441_MATRIX4_H_
#define _CSCI441_MATRIX4_H_

#include <string>

class Matrix4{

	void init_to_identity(){
		for(int i=0; i<16; ++i){
			if     (i == 0)  values[i] = 1;
			else if(i == 5)  values[i] = 1;
			else if(i == 10) values[i] = 1;
			else if(i == 15) values[i] = 1;
			else values[i] = 0;
		}
	}

	public:
		float values[16];

	Matrix4(){
		init_to_identity();
	}

	std::string to_string() const {
		std::ostringstream os;
		for(int i=0; i<16; ++i){
			os << values[i] << " ";
			if( (i+1)%4 == 0 ) os << "\n";
		}
		return os.str();
	}

	void rotate_x(float angle){
		values[5]  =  cos(angle);
		values[6]  = -sin(angle);
		values[9]  =  sin(angle);
		values[10] =  cos(angle);
	}

	void rotate_y(float angle){
		values[0]  =  cos(angle);
		values[2]  = -sin(angle);
		values[8]  =  sin(angle);
		values[10] =  cos(angle);
	}

	void rotate_z(float angle){
		values[0] =  cos(angle);
		values[1] = -sin(angle);
		values[4] =  sin(angle);
		values[5] =  cos(angle);
	}

	void scale(float factor){
		values[0]  = factor;
		values[5]  = factor;
		values[10] = factor;
	}

	void translate(float deltaX, float deltaY, float deltaZ){
		values[12] = deltaX;
		values[13] = deltaY;
		values[14] = deltaZ;
	}

	Matrix4 multiply(Matrix4 second){
		Matrix4 product;
		product.values[0]  = values[0]*second.values[0]  + values[1]*second.values[4]  + values[2]*second.values[8]   + values[3]*second.values[12];
		product.values[1]  = values[0]*second.values[1]  + values[1]*second.values[5]  + values[2]*second.values[9]   + values[3]*second.values[13];
		product.values[2]  = values[0]*second.values[2]  + values[1]*second.values[6]  + values[2]*second.values[10]  + values[3]*second.values[14];
		product.values[3]  = values[0]*second.values[3]  + values[1]*second.values[7]  + values[2]*second.values[11]  + values[3]*second.values[15];
		product.values[4]  = values[4]*second.values[0]  + values[5]*second.values[4]  + values[6]*second.values[8]   + values[7]*second.values[12];
		product.values[5]  = values[4]*second.values[1]  + values[5]*second.values[5]  + values[6]*second.values[9]   + values[7]*second.values[13];
		product.values[6]  = values[4]*second.values[2]  + values[5]*second.values[6]  + values[6]*second.values[10]  + values[7]*second.values[14];
		product.values[7]  = values[4]*second.values[3]  + values[5]*second.values[7]  + values[6]*second.values[11]  + values[7]*second.values[15];
		product.values[8]  = values[8]*second.values[0]  + values[9]*second.values[4]  + values[10]*second.values[8]  + values[11]*second.values[12];
		product.values[9]  = values[8]*second.values[1]  + values[9]*second.values[5]  + values[10]*second.values[9]  + values[11]*second.values[13];
		product.values[10] = values[8]*second.values[2]  + values[9]*second.values[6]  + values[10]*second.values[10] + values[11]*second.values[14];
		product.values[11] = values[8]*second.values[3]  + values[9]*second.values[7]  + values[10]*second.values[11] + values[11]*second.values[15];
		product.values[12] = values[12]*second.values[0] + values[13]*second.values[4] + values[14]*second.values[8]  + values[15]*second.values[12];
		product.values[13] = values[12]*second.values[1] + values[13]*second.values[5] + values[14]*second.values[9]  + values[15]*second.values[13];
		product.values[14] = values[12]*second.values[2] + values[13]*second.values[6] + values[14]*second.values[10] + values[15]*second.values[14];
		product.values[15] = values[12]*second.values[3] + values[13]*second.values[7] + values[14]*second.values[11] + values[15]*second.values[15];
		return product;
	}

	void set(int index, float value){
		values[index] = value;
	}

	void clear(){
		init_to_identity();
	}

};

#endif